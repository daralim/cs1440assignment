//
// Created by Dara Lim on 4/20/17.
//

#ifndef GENERICDICTIONARYOPTIMIZATION_KEYVALUE_H
#define GENERICDICTIONARYOPTIMIZATION_KEYVALUE_H
#include<string>

template<typename Key, typename Value>
class KeyValue {
    private:
        Key m_key;
        Value m_value;

    public:
        KeyValue(){

        }
        KeyValue(Key key,  Value value){
        m_key = key;
        m_value = value;
    }
    void setKey(Key inputKey) {
        m_key = inputKey;
    }
    void setValue(Value inputValue){
        m_value = inputValue;
    }
    Key getKey() const { return m_key; }
    Value getValue() const { return m_value; }


};


#endif //GENERICDICTIONARYOPTIMIZATION_KEYVALUE_H
