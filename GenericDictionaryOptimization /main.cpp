#include <iostream>
#include "Dictionary.h"
int main() {

    int choice;
    Dictionary<std::string, std::string> myDictionary;
    myDictionary.insert(myDictionary.root, "HairColor", "Black");
    myDictionary.insert(myDictionary.root, "EyeColor", "Brown");
    myDictionary.insert(myDictionary.root, "FavoriteClass", "CS1440");
    do {
        std::cout << std::endl
                  << " Welcome to my Dictionary " << std::endl
                  << " 1 - Add Key Value Pair" << std::endl
                  << " 2 - Remove the Dictionary by key" << std::endl
                  << " 3 - Print the Dictionary " << std::endl
                  << " 4 - get the key and value you need" << std::endl
                  << " 5 - Exit the program " << std::endl
                  << " Enter your choice " << std::endl;
        std::cin >> choice;
        std::cin.ignore();
        std::string key;
        std::string value;
        std::string id;
        switch (choice) {
            case 1: {
                std::cout << "Enter the key: ";
                std::getline(std::cin,key);
                std::cout<<std::endl;
                std::cout<< "Enter the Value: ";
                std::getline(std::cin,value);
                std::cout<<std::endl;
                std::cout<<"The key and value have been added"<<std::endl;
                myDictionary.insert(myDictionary.root, key, value);
                break;
            }
            case 2: {
                std::cout << "Enter the key you want to remove " << std::endl;
                std::cin >> key;
                myDictionary.remove(myDictionary.root, key);
                std::cout << " The key you entered has been removed." <<std::endl;
                break;
            }
            case 3: {
                myDictionary.displayPreOrder(myDictionary.root);
                break;
            }
            case 4: {
                std::cout << "Enter the key you want to get:" << std::endl;
                std::cin >> key;
                if(!myDictionary.get(myDictionary.root, key)){
                    std::cout << "The key is not in the tree." <<std::endl;
                    std::cout << "" <<std::endl;
                }
                else
                {
                   std::cout <<"Here is the key and value in dictionary:"<< std::endl;
                   std::cout << std::endl;
                   myDictionary.displayPreOrder(myDictionary.root);

                }


                break;
            }
            case 5: {
                return 0;
            }
        }
    } while (choice != 5);

}