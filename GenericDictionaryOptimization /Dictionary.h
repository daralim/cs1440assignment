//
// Created by Dara Lim on 4/4/17.
//

#ifndef GENERICDICTIONARY_DICTIONARY_H
#define GENERICDICTIONARY_DICTIONARY_H

#include <ostream>
#include "KeyValue.h"
#include<vector>
template <typename Key, typename Value>
class Dictionary
{
private:
    class dictionaryNode
    {
    public:
        friend class Dictionary;
        Key key;
        Value value;
        dictionaryNode *left;
        dictionaryNode *right;
        dictionaryNode(Key data, Value data1, dictionaryNode *left1 = NULL, dictionaryNode *right1 = NULL)
        {
            key = data;
            value = data1;
            left = left1;
            right = right1;
        }
    };

public:
    Dictionary() {root = nullptr;}
    ~Dictionary();
    void cleanTree(dictionaryNode*);
    bool insert(dictionaryNode *&node, Key key, Value value);
    bool get(dictionaryNode *node, Key key);
    void remove(dictionaryNode *&node, Key key);
    void makeDeletion(dictionaryNode *&node);
    void displayPreOrder(dictionaryNode *);
    dictionaryNode *root;
};
//clean the node function
template <typename Key, typename Value>
void Dictionary<Key, Value>::cleanTree(dictionaryNode *node)
{
    if (node==NULL)
        return;
    if (node->left)
        cleanTree(node->left);
    if (root->right)
        cleanTree(node->right);
    delete node;
}
//Destructor
template <typename Key, typename Value>
Dictionary<Key, Value>::~Dictionary(){
    cleanTree(root);
}
//function insert
template <typename Key, typename Value>
bool Dictionary<Key, Value>::insert(dictionaryNode *&node, Key key1, Value value1)
{
    if(node==nullptr){
        node = new dictionaryNode(key1, value1);
        return true;
    }
    if (key1 == node->key)
        return false;
    if(key1 > node->key)
        return insert(node->right, key1, value1);
    if(key1 < node->key)
        return insert(node->left, key1, value1);
    else return false;
}

//function search
template <typename Key, typename Value>
bool Dictionary<Key, Value>::get(dictionaryNode *node, Key key1)
{
    while (node)
    {
        if (key1 == node->key)
            return true;
        else if (key1 < node->key)
            return get(node->left, key1);
        else
            return get(node->right, key1);
    }
    return false;
}

//function display
template <typename Key, typename Value>
void Dictionary<Key, Value>::displayPreOrder(dictionaryNode *node)
{
    if(node)
    {
        displayPreOrder(node->left);
        std::cout << node->key << ", " << node->value << std::endl;
        displayPreOrder(node->right);
    }
}

//function remove
template <typename Key, typename Value>
void Dictionary<Key, Value>::remove(dictionaryNode *&node, Key key1)
{
    if(node == NULL) return;
    if(key1 < node->key)
        remove(node->left, key1);
    else if(key1 > node->key)
        remove(node->right, key1);
    else
        makeDeletion(node);
}

//function makeDeletion
template<typename Key, typename Value>
void Dictionary<Key, Value>::makeDeletion(dictionaryNode *&node)
{
    dictionaryNode *nodeToDelete = node;
    dictionaryNode *attachPoint;
    if(node->right == NULL)
    {
        node = node->left;
    }
    else if(node->left == NULL)
    {
        node = node->right;
    }
    else
    {
        attachPoint = node->right;
        while(attachPoint->left != NULL)
            attachPoint = attachPoint->left;
        attachPoint->left = node->left;
        node = node->right;
        delete nodeToDelete;
    }
}



#endif //GENERICDICTIONARY_DICTIONARY_H
